package ru.t1.aksenova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.aksenova.tm")
public class ApplicationConfiguration {

}
