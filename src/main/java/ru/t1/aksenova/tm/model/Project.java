package ru.t1.aksenova.tm.model;


import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.aksenova.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Project {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    public Project() {
    }

    public Project(
            final String name,
            final Status status
    ) {
        this.name = name;
        this.status = status;
    }

}
